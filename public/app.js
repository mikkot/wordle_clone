const keyboardLine1 = "qwertyuiop";
const keyboardLine2 = "asdfghjkl";
const keyboardLine3 = "zxcvbnm";

const elementIDs = {
  SCORE: '#score',
  WIN_RATIO: '#win-ratio',
};

const elementClasses = {
  WORDS_CONTAINER: '.words-container',
}

const storageKeys = {
  SCORE: 'score',
  TRIES: 'tries',
};

const colors = {
  CORRECT: '#00aa39',
  PRESENT: '#c0c005',
  WRONG: 'black',
};

let correctWord = [];
let currentLetterIndex = 0;
let currentLetterRow = 0;
let maximumTries = 5;
let winCondition = false;
let currentLetters = [];
const LETTERS = 5;

const keyboard = document.querySelector(".keyboard");
const wordsContainer = document.querySelector(elementClasses.WORDS_CONTAINER);

// LocalStorage
const loadAndDisplayScore = () => {
  const score = localStorage.getItem(storageKeys.SCORE) ? localStorage.getItem(storageKeys.SCORE) : 0;
  document.querySelector(elementIDs.SCORE).innerHTML = score;
}

const incrementScore = () => {
  const oldScore = localStorage.getItem(storageKeys.SCORE) ? localStorage.getItem(storageKeys.SCORE) : 0;
  localStorage.setItem(storageKeys.SCORE, parseInt(oldScore) + 1);
}

const incrementTries = () => {
  const currentTries = localStorage.getItem(storageKeys.TRIES) ? localStorage.getItem(storageKeys.TRIES) : 0;
  localStorage.setItem(storageKeys.TRIES, parseInt(currentTries) + 1);
}

const calculateAndDisplayWinRatio = () => {
  const currentTries = localStorage.getItem(storageKeys.TRIES) ? parseInt(localStorage.getItem(storageKeys.TRIES)) : 0;
  const currentWins = localStorage.getItem(storageKeys.SCORE) ? parseInt(localStorage.getItem(storageKeys.SCORE)) : 0;

  let ratio = 0;
  // If either tries or wins are 0, we can't do a calculation
  if(currentTries === 0) {
    ratio = 100;
  } else if(currentTries !== 0 && currentWins !== 0) {
    ratio = Math.round(currentWins / currentTries * 100);
  }

  document.querySelector(elementIDs.WIN_RATIO).innerHTML = `${ratio}%`;
}

// Utilities
const chooseWordAtRandom = (list) => {
  const listAsArray = list.split('\n');
  return listAsArray[Math.floor(Math.random() * listAsArray.length)].toUpperCase().split('');
}


// Game mechanics
const isLetterInCorrectWord = (letter) => {
  return correctWord.includes(letter);
}

const didPlayerWin = () => {
  // If all currentletters match correctWord, player has won.
  console.log('Checking if player won...');
  console.log('Selected: ', currentLetters);
  console.log('Correct: ', correctWord);
  for(let i = 0; i<correctWord.length; i++) {
    if(currentLetters[i] !== correctWord[i]) return false
  }
  return true;
}

const resetAllKeys = () => {
  console.log('Resetting keys...');
  keyboardLine1.toUpperCase().split('')
    .forEach(key => document.querySelector(`#${key}`).style.backgroundColor = "gray");
  keyboardLine2.toUpperCase().split('')
    .forEach(key => document.querySelector(`#${key}`).style.backgroundColor = "gray");
  keyboardLine3.toUpperCase().split('')
    .forEach(key => document.querySelector(`#${key}`).style.backgroundColor = "gray");
}

const resetAllLetters = () => {
  console.log('Resetting letters...');
  // There are as many rows as there are allowed tries to guess
  for(let i = 0; i<maximumTries; i++) {
    for(let j = 0; j<LETTERS; j++) {
      const letter = document.getElementById(`${i}${j}`)
      if(letter) {
        letter.innerHTML = '';
        letter.style.backgroundColor = '';
      }
    }
  }
}

const reset = () => {
  console.log('Resetting game...');
  correctWord = chooseWordAtRandom(fiveLetterWords);
  currentLetterIndex = 0;
  currentLetterRow = 0;
  winCondition = false;
  currentLetters = [];

  resetAllKeys();
  resetAllLetters();
  document.getElementById('reset-btn').remove();
}

const showResetButton = () => {
  const button = document.createElement('div');
  button.setAttribute('class', 'key');
  button.setAttribute('id', 'reset-btn');
  button.innerHTML = 'RESET';
  button.onclick = () => reset();
  document.querySelector('.keyboard').appendChild(button);
}

// Input actions
const erase = () => {
  if(!currentLetters.length) return;
  currentLetters.pop();
  document.getElementById(`${currentLetterRow}${currentLetterIndex - 1}`).innerHTML = '';
  currentLetterIndex--;
}

const onLetterClick = (event) => {
  console.log('Clicked key: ', event.target.innerHTML);
  if(currentLetterIndex >= correctWord.length) return;

  // Set the letter to the letter row
  document
    .getElementById(`${currentLetterRow}${currentLetterIndex}`)
    .innerHTML = event.target.innerHTML;
  currentLetterIndex++;
  currentLetters.push(event.target.innerHTML);
  console.log('Current selected letters: ', currentLetters);
}

const onEnterClick = (event) => {
  // Prevent multiple submissions of the same correct word
  if(winCondition) return;

  // We check that user has given enough letters
  if(currentLetters.length < correctWord.length) {
    // Somehow inform user that they are missing letters
    return;
  }

  // Highlight the keys and letters the player used
  // Letter in word and in correct place = green
  // Letter in word but wrong place = yellow
  // Letter not in word = black
  for(let i = 0; i<correctWord.length; i++) {
    if(correctWord[i] === currentLetters[i]) {
      // Highlight according key green
      const key = document.querySelector(`#${currentLetters[i]}`);
      key.style.backgroundColor = colors.CORRECT;

      // Highlight according letter green
      document.getElementById(`${currentLetterRow}${i}`).style.backgroundColor = colors.CORRECT;
    } else if(correctWord.includes(currentLetters[i])) {
      // Highlight accordinly key yellow
      const key = document.querySelector(`#${currentLetters[i]}`);
      key.style.backgroundColor = colors.PRESENT;

      // Highlight according letter yellow
      document.getElementById(`${currentLetterRow}${i}`).style.backgroundColor = colors.PRESENT;
    } else {
      // Highlight key black
      const key = document.querySelector(`#${currentLetters[i]}`);
      key.style.backgroundColor = colors.WRONG;

      // Highlight according letter black
      document.getElementById(`${currentLetterRow}${i}`).style.backgroundColor = colors.WRONG;
    }

    document.getElementById(`${currentLetterRow}${i}`).innerHTML = currentLetters[i];
  }

  if(didPlayerWin()) {
    console.log('PLAYER WON!');
    winCondition = true;
    incrementScore();
    loadAndDisplayScore();
    incrementTries();
    showResetButton();
    calculateAndDisplayWinRatio();
    return;
  }

  // Reset the current word guess
  currentLetters = [];
  currentLetterIndex = 0;

  currentLetterRow++;

  // Check if player has reached the limit of guesses
  if(currentLetterRow >= maximumTries) {
    incrementTries();
    showResetButton();
    calculateAndDisplayWinRatio();
  }
}


// App page setup
const createKeyboardLine = (letters, rowNumber) => {
  const row = document.createElement('div');
  row.setAttribute('class', `row ${rowNumber}`);
  document.querySelector('.keyboard').appendChild(row);

  letters.toUpperCase().split('').forEach(char => {
    const key = document.createElement('div');
    key.innerHTML = char;
    key.setAttribute('class', 'key');
    key.setAttribute('id', char);
    key.onclick = (event) => onLetterClick(event);
    row.appendChild(key);
  })
}

const createBackspace = () => {
  const backspaceKey = document.createElement('div');
  backspaceKey.innerHTML = '<-';
  backspaceKey.setAttribute('class', 'key');
  backspaceKey.onclick = (event) => erase(event);
  keyboard.appendChild(backspaceKey);
}

const createEnterKey = () => {
  const enterKey = document.createElement('div');
  enterKey.innerHTML = "ENTER";
  enterKey.setAttribute('class', 'key');
  enterKey.onclick = (event) => onEnterClick(event);
  keyboard.appendChild(enterKey);
}

const createLetterLine = (amount, rows) => {
  maximumTries = rows;

  for(let i = 0; i<rows; i++) {
    for(let j = 0; j<amount; j++) {
      const letter = document.createElement('div');
      letter.setAttribute('class', 'letter');
      letter.setAttribute('id', `${i}${j}`)
      wordsContainer.appendChild(letter);
    }
  }
}

const fetchWordList = (url) => {
  const list = [];
  if(!list.length) {
    throw new Error('No words to pick from!');
  }
  return 
}



// Setup of the app

// Create first keyboard line
console.log('Creating first keyboard row');
console.log(fiveLetterWords);
createKeyboardLine(keyboardLine1, 'first');
createKeyboardLine(keyboardLine2, 'second');
createKeyboardLine(keyboardLine3, 'third');
createEnterKey();
createBackspace();

console.log('Creating first line of letters...');
// Letters, rows
createLetterLine(5, 6);

// Set the word to guess
correctWord = chooseWordAtRandom(fiveLetterWords);

// Enable keyboard for better UX on desktop
const onKeyPressed = (event) => {
  // Enable checking current word with ENTER press
  if (event.key.toUpperCase() === 'ENTER') {
    onEnterClick();
    return;
  }

  // Enable backspace
  if (event.key.toUpperCase() === 'BACKSPACE') {
    erase();
    return;
  }

  const key = event.key.toUpperCase();
  // Disregard keys that are not characters in english alphabet
  if(key.length > 1) return;
  if(!(key.charCodeAt(0) > 64 && key.charCodeAt(0) < 91)) return;

  // We reuse our existing function that handles our virtual keyboard clicks
  const customEvent = { target: { innerHTML: key } };
  onLetterClick(customEvent);
};

document.addEventListener('keydown', onKeyPressed);
loadAndDisplayScore();
calculateAndDisplayWinRatio();